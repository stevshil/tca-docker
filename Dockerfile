FROM steve353/centos:7.4.1708
MAINTAINER Neueda and TPS
LABEL Version="1.0.0"
LABEL Description="Docker container to run Martins trade app"
RUN mkdir /app && chmod 777 /app
# Download Sun Java
RUN yum -y update
RUN yum -y install wget

# Install JConnector and Sun Java 8
RUN wget -nv 'https://www.dropbox.com/s/lqqp8zjc1ibmk8e/jdk-8u131-linux-x64.rpm?dl=0' -O /tmp/jdk-8u131-linux-x64.rpm
RUN wget 'https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.42.tar.gz' -O /tmp/mysql-connector-java-5.1.42.tar.gz
RUN cd /tmp && tar xvf mysql-connector-java-5.1.42.tar.gz && mv /tmp/mysql-connector-java-5.1.42/mysql-connector* /app
RUN yum -y install /tmp/jdk-8u131-linux-x64.rpm

# Install the App
COPY target/trade-app-1.0.0-exec.jar /app/target/trade-app-1.0.0.jar
COPY target/trade-app-1.0.0-exec.jar /app/target/injector-1.0.0.jar
COPY src/ /app/
COPY bin/start_trades /usr/bin/start_trades
RUN chmod +x /usr/bin/start_trades
COPY application.properties /app/application-prod.properties
COPY application-injector.properties /app/application-injector.properties
EXPOSE 8080 7091
ENTRYPOINT ["/usr/bin/start_trades"]
